<?php
// Function to load ratings from the JSON file
function loadRatings() {
    $filename = 'ratings.json';

    if (file_exists($filename)) {
        $json_data = file_get_contents($filename);
        if ($json_data === false) {
            // Handle the error, e.g., log or display an error message
            die('Error reading JSON file');
        }

        $ratings = json_decode($json_data, true);
        if ($ratings === null) {
            // Handle the error, e.g., log or display an error message
            die('Error decoding JSON data');
        }

        return $ratings;
    } else {
        return ['pictures' => []]; // Use 'pictures' as the top-level key
    }
}

// Function to save ratings to the JSON file
function saveRatings($ratings) {
    $filename = 'ratings.json';
    $json_data = json_encode($ratings, JSON_PRETTY_PRINT);
    if ($json_data === false) {
        // Handle the error, e.g., log or display an error message
        die('Error encoding JSON data');
    }

    $result = file_put_contents($filename, $json_data);
    if ($result === false) {
        // Handle the error, e.g., log or display an error message
        die('Error writing to JSON file');
    }
}

// Function to add or update a rating
function addOrUpdateRating($pictureId, $raterId, $rating) {
    $ratings = loadRatings();
    $found = false;

    foreach ($ratings['pictures'] as &$picture) {
        $json = json_encode($picture, JSON_PRETTY_PRINT);
        $logId = $picture['picture_id'];
        $logData = "JSON ID: $logId\npictureId: $pictureId\n";
        file_put_contents('debug.log', $logData, FILE_APPEND);
        if ($picture['picture_id'] == $pictureId) {
                    $json = json_encode($picture, JSON_PRETTY_PRINT);
                    $logData = "Picture found: $json\n";
                    file_put_contents('debug.log', $logData, FILE_APPEND);
            foreach ($picture['ratings'] as &$item) {
                if ($item['rater_id'] == $raterId) {
                    $item['rating'] = $rating;
                    $json = json_encode($item, JSON_PRETTY_PRINT);
                    $logData = "Item: $json\n";
                    file_put_contents('debug.log', $logData, FILE_APPEND);
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                $picture['ratings'][] = [
                    'rater_id' => $raterId,
                    'rating' => $rating
                ];
                $found = true;
            }
        }

    }
    if (!$found) { // Create new entry
        $ratings['pictures'][] = [
        'picture_id' => $pictureId,
        'ratings' => [[
                'rater_id' => $raterId,
                'rating' => $rating
            ]]];
    }
    saveRatings($ratings);
}

// Function to get the rating for a given picture and rater ID
function getRating($pictureId, $raterId) {
    $ratings = loadRatings();

    foreach ($ratings['pictures'] as $picture) {
        if ($picture['picture_id'] == $pictureId) {
            foreach ($picture['ratings'] as $item) {
                if ($item['rater_id'] == $raterId) {
                    return $item['rating'];
                }
            }
        }
    }

    return null; // Return null if no rating is found
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['get_rating'])) {
    $pictureId = $_POST['picture_id'];
    $raterId = $_POST['rater_id'];
    $rating = getRating($pictureId, $raterId);

    // Return the rating as JSON response
    echo json_encode(['rating' => $rating]);
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['rating'])) {
    $pictureId = $_POST['picture_id'];
    $raterId = $_POST['rater_id'];
    $rating = $_POST['rating'];

    // Log the data to a text file
    $logData = "Picture ID: $pictureId, Rater ID: $raterId, Rating: $rating\n";
    file_put_contents('debug.log', $logData, FILE_APPEND);

    addOrUpdateRating($pictureId, $raterId, $rating);
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $filename = 'img/images.txt';

    if (file_exists($filename)) {
        $filenames = file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        echo json_encode(['filenames' => $filenames]);
    } else {
        echo json_encode(['filenames' => []]);
    }
}

?>
