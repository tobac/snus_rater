<?php
// Function to get the rating for a given picture and rater ID
function getRating($pictureId, $raterId) {
    $ratings = loadRatings();

    foreach ($ratings['pictures'] as $picture) {
        if ($picture['picture_id'] == $pictureId) {
            foreach ($picture['ratings'] as $item) {
                if ($item['rater_id'] == $raterId) {
                    return $item['rating'];
                }
            }
        }
    }

    return null; // Return null if no rating is found
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $pictureId = $_POST['picture_id'];
    $raterId = $_POST['rater_id'];
    $rating = getRating($pictureId, $raterId);

    // Return the rating as JSON response
    echo json_encode(['rating' => $rating]);
}
?>
